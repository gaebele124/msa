# -------------------------------------
#
# G r u n d l e g e n d e s
#
# -------------------------------------        
print("to initialise call:")
print("  set_alpha_beta_maxdeg(alpha,beta,maxdeg)")
print("  MSA=init_MSA(p,L)")

def Trafo(a,b): # (a,b) -> (-1,1)
    return (2*(x-a)/(b-a)-1).function(x)

def Trafo_Inv(a,b): # (-1,1) -> (a,b)
    return ((b-a)*(x+1)/2+a).function(x)

def wei(x):
    return (1-x)^alpha*(1+x)^beta

def wei_plot(f=1):
    return plot(wei(x)*f,linestyle="-.")

def set_alpha_beta_maxdeg(_alpha,_beta,_maxdeg):
    global alpha,beta,maxdeg
    alpha = _alpha
    beta  = _beta
    maxdeg = _maxdeg
    init_monom_ints(2*_maxdeg)

#from mpmath import appellf1, mp
#mp.dps = 50
#mp.pretty = true
# def gram_appell(a,b,n=3,ring=RealField(128):
#     mr = [m(b)-m(a) for m in monom_ints]
#     ring=RealField(128) für beliebige alpha,beta > -1
#     mr = [(b^(m+1)*appellf1(m+1,-alpha,-beta,m+2,b,-b)-a^(m+1)*appellf1(m+1,-alpha,-beta,m+2,a,-a))/(m+1)
#          for m in [0..2*n]]
#     return matrix(ring,n,n, lambda i,j: mr[i+j])

# generates Gramian matrix for interval (a,b) and n base functions
def gram(a,b,n=3,ring=QQ): # alternativer Ring: SR für symbolische Auswertung
    mr = [m(b)-m(a) for m in monom_ints]
    return matrix(ring,n,n, lambda i,j: mr[i+j])

def vector_to_polynom(v):
    return sum(c*x^i for i,c in enumerate(v))

def vector_to_piecewise(v,u,U):
    uU = (u+U)/2
    return piecewise([[RealSet(u,uU),vector_to_polynom(v[0])], [RealSet(uU,U),vector_to_polynom(v[1])]])

# -------------------------------------
#
# J a c o b i  -  P o l y n o m i a l s
#
# -------------------------------------

def P(n,a,b,x):
    var('m')
    return gamma(a+n+1)/(n.factorial()*gamma(a+b+n+1)) \
    * sum(
    n.factorial()*gamma(a+b+n+m+1) \
    /(m.factorial()*(n-m).factorial()*gamma(a+m+1)) \
    *((x-1)/2)^m,
    m,0,n)

def an(n,alpha,beta):
    if n==0:
        return (beta-alpha)/(alpha+beta+2)
    return (beta^2-alpha^2)/((2*n+alpha+beta)*(2*n+alpha+beta+2))

def bn(n,alpha,beta):
    if n==1:
        return 4*(1+alpha)*(1+beta)/((2+alpha+beta)^2*(3+alpha+beta))
    return 4*n*(n+alpha)*(n+beta)*(n+alpha+beta)/((2*n+alpha+beta-1)*(2*n+
    alpha+beta)^2*(2*n+alpha+beta+1))

def P3(n,alpha,beta,x): #inefficient since an(...) gets called multiple times
    # NOTE: P3 not(!) normalized such that P3(n,alpha,beta,1) = binom(n+alpha,n)
    if n==0:
        return 1
    if n==1:
        return x-an(0,alpha,beta)
    return (x-an(n-1,alpha,beta))*P3(n-1,alpha,beta,x) - bn(n-1,alpha,beta)*P3(n-2,alpha,beta,x)

# ----------------------------------------------
#
# J a c o b i  -  P o l y n o m i a l    r o o t s
#
# ----------------------------------------------

def A3(n,alpha,beta, field=RDF):
    var('A','b')
    A = diagonal_matrix(field, [an(k,alpha,beta) for k in [0..n-1]], sparse=false)
    for k in [1..n-1]:
        b = sqrt(bn(k,alpha,beta))
        A[k,k-1] = b
        A[k-1,k] = b
    return A

def calc_mu0(alpha,beta):
    # mu0 = integral((1-x)^alpha*(1+x)^beta,x,-1,1)    
    return (2^beta*2^(alpha+1) / (alpha+1)*hypergeometric((alpha+1,-beta),(alpha+2,),1)).n()


Tz(n,k) = cos(pi*(k+1/2)/n)   #zeroes of Chebyshev polynomials first kind   alpha=beta=-1/2
Uz(n,k) = cos(pi*(k+1)/(n+1)) #zeroes of Chebyshev polynomials second kind   alpha=beta=1/2

def P_roots(n,alpha,beta):
    return A3(n,alpha,beta).eigenvalues()



# ----------------------------------------------
#
# G a u s s - J a c o b i    Q u a d r a t u r e
#
# ----------------------------------------------

def GJ_quad_koeff(n,alpha,beta,mu0 = 0,field=RDF):
    # field=RDF -> numerically, QQbar -> exact (takes infinite time)
    if(mu0 == 0):
        mu0 = calc_mu0(alpha,beta)

    ev  = A3(n,alpha,beta,field).eigenvectors_left()

    #        ev[k][0] is eigenvalue,  ev[k][1] is list of corresponding eigenvectors
    #          |                      eigenvector is normalized if A3(n,alpha,beta) uses RDF
    #          v                      eigenvector not normalized if A3(n,alpha,beta) uses QQbar
    return [(ev[k][0], (mu0*ev[k][1][0].normalized()[0]^2).n()) for k in [0..n-1]]

def GJ_quad(f,xH,u=-1,U=1): #xH = GJ_quad_koeff(...)
    n  = len(xH)
    xj = [xh[0] for xh in xH]
    hj = [xh[1] for xh in xH]
    return sum(hj[i]* (f(xj[i]) if xj[i] in RealSet([u,U]) else 0) for i in [0..n-1])



# ----------------------------------
#
# I n t e g r a t i o n
#
# ----------------------------------

# Integral weighted via adaptive Gauss-Kronrod quadrature 
def Iw(f,u=-1,U=1): #!alpha,beta>=0
    if(piecewise.in_operands(f)):    
        g = piecewise_from_operands(f)
        doms = g.domains()
        return sum(Iw(
                        g.expressions()[i].function(x),
                        max(doms[i][0].lower(),min(u,doms[i][0].upper())),
                        min(doms[i][0].upper(),max(U,doms[i][0].lower()))
                    )for i in [0..len(doms)-1])
    else:
        return numerical_integral((1-x)^alpha*(1+x)^beta*f(x),u,U, algorithm='qag')[0]

# Integral weighted exact
def Iwe(f,u=-1,U=1):
    if(piecewise.in_operands(f)):    
        g = piecewise_from_operands(f)
        doms = g.domains()
        return sum(Iwe(
                        g.expressions()[i].function(x),
                        max(doms[i][0].lower(),min(u,doms[i][0].upper())),
                        min(doms[i][0].upper(),max(U,doms[i][0].lower()))
                    )for i in [0..len(doms)-1])
    else:
        return integral((1-x)^alpha*(1+x)^beta*f(x),x,u,U)

# indefinite integral of q(x)*(x+y)^n*(x+z)^m, q polynomial
def binom_pol_int(y,z,q,n,m,x0=x):
    var('i,j')
    return x0^(n+m+1)*sum(sum(sum(
            binomial(n,i)*binomial(m,j) * y^i * z^j * ck * x0^(k-i-j)
            / (n+m+k-i-j+1)
        ,i,0,n) ,j,0,m) for (ck,k) in q.coefficients(var('x')))

# saves indefinite integrals of x^i * w(x),  n > 2*(p-1)
def init_monom_ints(n):
    global monom_ints
    monom_ints = [(-1)^alpha * binom_pol_int(-1,1,x^i,alpha,beta) for i in [0..n]]

# ----------------------------------
#
# p i e c e w i s e
#
# ----------------------------------

from sage.symbolic.operators import add_vararg, mul_vararg

def piecewise_from_operands(ex):
    if not piecewise.in_operands(ex):
        return ex
    
    if hasattr(ex, 'pieces'):
        return ex

    if ex.operator() is operator.pow:
        base = ex.operands()[0]
        expo = ex.operands()[1]
        if Expression.is_integer(expo):
            return piecewise_from_operands(base)^expo

    if ex.operator() is mul_vararg   or   ex.operator() is add_vararg:
        operands = ex.operands()
        OP       = ex.operator()

        # call piecewise_from_operands recursively on operands and on pairwise operands
        if len(operands) > 2:
            # src/sage/symbolic/operators.py  -> mul_vararg
            first = 0
            # find one piecewise operand
            for i in [0..len(operands)-1]:
                if(piecewise.in_operands(operands[i])):
                    first = operands.pop(i)
                    break
            first = piecewise_from_operands(first)

            for r in operands:
                r = piecewise_from_operands(r)
                first = piecewise_from_operands(OP(first,r))
            return first

        else:
            f = ex.operands()[0]
            f = piecewise_from_operands(f)
            g = ex.operands()[1]
            g = piecewise_from_operands(g)
            fpieces = gpieces = []
            if hasattr(f, 'pieces')  and not hasattr(g, 'pieces'):
                fpieces = f.pieces()
                gpieces = [piecewise([(f.domain(), g)],var=x)]
            if hasattr(g, 'pieces')  and not hasattr(f, 'pieces'):
                gpieces = g.pieces()
                fpieces = [piecewise([(g.domain(), f)],var=x)]
            if hasattr(f, 'pieces')    and   hasattr(g, 'pieces'):
                fpieces = f.pieces()
                gpieces = g.pieces()

            return piecewise([(fpi.domain() & gpi.domain(), OP(fpi.expressions()[0],gpi.expressions()[0]))
                    for fpi in fpieces for gpi in gpieces],var=x)

def piecewise_simplify(f):
    return piecewise([(p.domain(), simplify(p.expressions()[0])) for p in f.pieces()],var=x)

def simp(f):
    if piecewise.in_operands(f):
        return piecewise_simplify(piecewise_from_operands(f))
    else:
        return simplify(f)

def piecewise_trafo(f,a,b):
    T=Trafo(a,b)
    Tinv=Trafo_Inv(a,b)
    return piecewise([(RealSet(Tinv(p.domain()[0].lower()), Tinv(p.domain()[0].upper()))
        , p.expressions()[0](T(x)).simplify_full()) for p in f.pieces()],var=x)

def piecewise_domain_plot(f,y1=-1,y2=1,rgbcolor=(0,0,0),alpha=1):
    fpw     = piecewise_from_operands(f)
    doms    = list(filter(lambda x: not x[0].is_point(),fpw.domains()))
    borders = list(set([x[0].lower() for x in doms]+[x[0].upper() for x in doms]))    
    return sum([line([(b,y1),(b,y2)],rgbcolor=rgbcolor,alpha=alpha) for b in borders])

# ----------------------------------
#
# G r a m - S c h m i d t
#
# ----------------------------------

def Gram_step(f, orthList, G0, G1=0):
    # piecewise
    if G1!=0:
        s  = [(f[0]*G0*o1 + f[1]*G1*o2)/(o1*G0*o1 + o2*G1*o2) for (o1,o2) in orthList]
        f1 = f[0] - sum(s[i]*orthList[i][0] for i in [0..len(orthList)-1])
        f2 = f[1] - sum(s[i]*orthList[i][1] for i in [0..len(orthList)-1])
        return [f1,f2]

    # not piecewise
    else:
        return f - sum((f*G0*o)/(o*G0*o)*o for o in orthList)

def orth_check(list,u,U):
    return [[Iwe(p*q,u,U).n() if hasattr(Iwe(p*q,u,U),'n') else Iwe(p*q,u,U)
        for p in list] for q in list]

# ------------------------------------------
#
# S k a l i e r u n g s f u n k t i o n e n
#
# ------------------------------------------

def Vlambda_base(p, Vlambda_u=-1, Vlambda_U=1, as_vector=false, as_piecewise=false, normalized=false):
    G0 = gram(Vlambda_u,Vlambda_U,p)
    base = []
    for i in [0..p-1]:
        v = vector([1 if i==j else 0 for j in [0..p-1]])
        base.append(Gram_step(v, base, G0))

    if(normalized):
        base = [v/sqrt(v*G0*v) for v in base]
    
    if(as_vector):
        return base
    else:
        base = [vector_to_polynom(v).function(x) for v in base]

    if(as_piecewise):
        return [piecewise(
            [[RealSet(Vlambda_u,Vlambda_U), b], [RealSet(-1,1)-RealSet(Vlambda_u,Vlambda_U), 0]]
            ) for b in base]
    else:
        return base

def Sl_plot(l,p,a=-1,b=1,legend=false,showweight=false,fontsize="medium"):
    T=Trafo(a,b)
    Tinv=Trafo_Inv(a,b)
    Vlambda_bases = []
    for i in [0..2^l - 1]:
        base = Vlambda_base(p, i*2*2^-l - 1, (i+1)*2*2^-l - 1, normalized=true)
        Vlambda_bases.append([f(T(x)) for f in base])
    legend_txt = [r"$\varphi_{\lambda,"+str(i)+"}$" for i in [0..p-1]] if legend else ''
    Pl = sum(plot(Vlambda_bases[i], Tinv(i*2*2^-l - 1), Tinv((i+1)*2*2^-l - 1), legend_label=legend_txt) for i in [0..2^l - 1])
    Pl.set_legend_options(font_size=fontsize)
    
    if(showweight):
        Pl = Pl + plot(wei(T(x)),a,b,linestyle="-.",legend_label=("$w$" if legend else ''))
        Pl.set_legend_options(handlelength=2)
    return Pl


# ----------------------------------
#
# W a v e l e t s
#
# ----------------------------------

def generate_wavelets(p, Vlambda_u, Vlambda_U, as_piecewise=true, normalized=false):
    G1 = gram(     Vlambda_u,         (Vlambda_u+Vlambda_U)/2, p)
    G2 = gram((Vlambda_u+Vlambda_U)/2,      Vlambda_U,         p)

    # Sl base on Vlambda
    V = Vlambda_base(p,Vlambda_u, Vlambda_U,as_vector=true)

    # make Sl base piecewise (2 coefficient vectors)
    V = [[v,v] for v in V]
    
    # init piecewise base of Sl+1\Sl and orthogonalize against Sl
    pre_wavelets = []
    for i in [0..p-1]:
        pw = [vector([1 if i==j else 0 for j in [0..p-1]]), vector([-1 if i==j else 0 for j in [0..p-1]])]
        pre_wavelets.append(Gram_step(pw, V, G1, G2))

    # orthogonalize base of Wl
    W = []
    for w in pre_wavelets:
        W.append(Gram_step(w, W, G1, G2))

    if(normalized):
        norms = [sqrt((w1*G1*w1 + w2*G2*w2)) for (w1,w2) in W]
        W = [[w1/norms[i],w2/norms[i]] for (i,(w1,w2)) in enumerate(W)]

    if(as_piecewise):
        return [vector_to_piecewise(w,Vlambda_u,Vlambda_U) for w in W]
    else:
        return W

def init_MSA(p,L):
    # singlescale base
    SL = []
    for k in [0..2^L-1]:
        u=-1+k*2^(-L+1)
        U=-1+(k+1)*2^(-L+1)
        SL = SL + Vlambda_base(p,u,U,normalized=true, as_piecewise=true)

    # multiscale base
    SlWl = Vlambda_base(p,-1,1,normalized=true, as_piecewise=true)
    for l in [0..L-1]:
        for k in [0..2^l-1]:
            u=-1+k*2^(-l+1)
            U=-1+(k+1)*2^(-l+1)
            W=generate_wavelets(p,u,U,normalized=true)
            W=[w.extension(0,RealSet(-1,1)-w.domain()) for w in W]
            SlWl = SlWl + W

    return [SL,SlWl,{"p":p,"L":L}]

def mt(f,MSA,eps):
    return multiscaletrafo_coeff(f,MSA,eps)

def mt_inv(coeffs,MSA,single=true,multi=true):
    return multiscaletrafo_inv(coeffs,MSA,single,multi)

def mt_plot(f,MSA,eps):
    return plot(mt_inv(mt(f,MSA,eps),MSA)+[f])

def mt_err(f,MSA,expos=[1..3]):
    errs = []
    fL     = mt_inv(mt(f,MSA,1e5),MSA,multi=false)
    err_fL = sqrt(Iw((fL-f)^2))
    for i in expos:
        eps = 10^(-i)
        C=mt(f,MSA,eps)
        di_count = sum([1 if c!=0 else 0 for c in C[1]])/len(C[1])
        fLeps=mt_inv(C,MSA,single=false)
        errs.append(["%.1e"%eps,"%.3e"%err_fL, "%.3e"%sqrt(Iw((fL-fLeps)^2)),di_count])
    return errs


def multiscaletrafo_coeff(f,MSA,eps):
    singlecoeffs = [Iw(f*s,-1,1) for s in MSA[0]]
    multicoeffs  = [Iw(f*s,-1,1) for s in MSA[1]]
    
    # prefactor of epsilon for thresholding
    L = MSA[2]["L"]
    p = MSA[2]["p"]
    lL = [2^-L]*p
    for l in [0..L-1]:
        for i in [0..2^l-1]:
            for j in [0..p-1]:
                lL.append(2^(l-L))
    
    # thresholding
    multicoeffs  = [c if abs(c) > eps*lL[i] else 0 for (i,c) in enumerate(multicoeffs)]

    return [singlecoeffs,multicoeffs]

def multiscaletrafo_inv(coeffs,MSA,single=true,multi=true):
    fL    = 0
    fLeps = 0

    if single:
        for i in [0..len(coeffs[0])-1]:
            fL    = piecewise_from_operands(fL+coeffs[0][i]*MSA[0][i])
    
    if multi:
        for i in [0..len(coeffs[1])-1]:
            fLeps = piecewise_from_operands(fLeps+coeffs[1][i]*MSA[1][i])

    if single and multi:
        return [fL, fLeps]
    elif single:
        return fL
    elif multi:
        return fLeps

